# Code of Conduct

**Note:** This was originally based on the Lemmy code of conduct. We will be adapting this further as we go. Yes, we know it was hella strict.

Last updated: July 26th, 2020

## Community Rules

- We are committed to providing a friendly, safe and welcoming environment for all, regardless of gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, religion, nationality, or other similar characteristic.
- Please avoid using homophobic, transphobic, racist, ableist, and other reactionary aliases or other nicknames that might detract from a friendly, safe and welcoming environment for all. "Ironic" prejudice is just prejudice.
- Please be kind and courteous to your fellow leftists. There’s no need to be mean or rude unless it's a chud.
- Respect that people have differences of opinion and that every leftist has a place in our community. Discussing differences in theory is fine and encouraged, just don't make it personal. Remember: Sectarianism is liberalism.
- Do not publish, comment, message, or post personally identifiable information about other users.
- We will exclude you from interaction if you insult, demean or harass anyone. That is not welcome behavior. In particular, we don’t tolerate behavior that excludes people in socially marginalized groups.
- Private harassment is also unacceptable. No matter who you are, if you feel you have been or are being harassed or made uncomfortable by a community member, please contact one of the channel ops or any of the Chapo moderation team immediately. Whether you’re a regular contributor or a newcomer, we care about making this community a safe place for you and we’ve got your back.
- Likewise any spamming, trolling, flaming, baiting or other attention-stealing behavior is not welcome.
- We are a platform that welcomes anyone who wants to be here in good faith. With that said, we are also an intentionally leftist platform; conservative and reactionary ideologies will not be tolerated here. 
- We do not platform fascists and refuse to engage in any form of debate with them on this platform. If you don't understand why engaging in debates with fascists is a terrible idea, go ask in one of the communities and/or use a search engine.
- Direct calls or claims to violence that target _specific living_ individuals are not permitted on Chapo.Chat. You can express your anger over landlords, slaveowners, etc, but try to phrase it in a way that focuses on the institution they represent. This policy also applies to any specific organizational bodies and groups (eg the DNC or NRA).

## Moderation

These are the policies for upholding our community’s standards of conduct. If you feel that a thread needs moderation, please contact the Chapo moderation team .

1. Remarks that violate the Chapo standards of conduct, including hateful, hurtful, oppressive, or exclusionary remarks, are not allowed. (Cursing is allowed, but never in a hateful manner.)
2. Remarks that moderators find inappropriate, whether listed in the code of conduct or not, are also not allowed.
3. Moderators will first respond to such remarks with a warning, at the same time the offending content will likely be removed whenever possible.
4. If the warning is unheeded, the user will be “kicked,” i.e., kicked out of the communication channel to cool off.
5. If the user comes back and continues to make trouble, they will be banned, i.e., indefinitely excluded.
6. Moderators may choose at their discretion to un-ban the user if it was a first offense and they offer the offended party a genuine apology.
7. If a moderator bans someone and you think it was unjustified, please take it up with that moderator, or with a different moderator, in private. Complaints about bans in-channel are not allowed.
8. Moderators are held to a higher standard than other community members. If a moderator creates an inappropriate situation, they should expect less leeway than others.
9. Egregious violations of our code of conduct or terms of service may result in an immediate ban.

In the Chapo community we strive to go the extra step to look out for each other. Don’t just aim to be technically unimpeachable, try to be your best self. In particular, avoid flirting with offensive or sensitive issues, particularly if they’re off-topic; this all too often leads to unnecessary fights, hurt feelings, and damaged trust; worse, it can drive people away from the community entirely.

And if someone takes issue with something you said or did, resist the urge to be defensive. Just stop doing what it was they complained about and apologize. Even if you feel you were misinterpreted or unfairly accused, chances are good there was something you could’ve communicated better — remember that it’s your responsibility to make others comfortable. Everyone wants to get along and we are all here first and foremost because we want to talk about cool technology. You will find that people will be eager to assume good intent and forgive as long as you earn their trust.

The enforcement policies listed above apply to all official Chapo venues; including git repositories under [our gitlab project](https://gitlab.com/chapo-sandbox/production) and all instances under [chapo.chat](https://chapo.chat). For other projects adopting the Rust Code of Conduct, please contact the maintainers of those projects for enforcement. If you wish to use this code of conduct for your own project, consider explicitly mentioning your moderation policy or making a copy with your own moderation policy so as to avoid confusion.

Adapted from the [Rust Code of Conduct](https://www.rust-lang.org/policies/code-of-conduct), which is based on the [Node.js Policy on Trolling](http://blog.izs.me/post/30036893703/policy-on-trolling) as well as the [Contributor Covenant v1.3.0](https://www.contributor-covenant.org/version/1/3/0/).

ChapoChat reserves the right to update this Code of Conduct without prior notification or warning at any time. For major changes, we might post in announcements, but have no obligation to do so. Users should check back periodically to remain familiar with current conduct standards.

That last paragraph is legal stuff and just means we can update the code of conduct and adapt as we go. 