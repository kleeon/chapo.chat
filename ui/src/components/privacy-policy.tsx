const PrivacyPolicy = () => (
  <div style={{ background: 'white' }}>
    <div class="mx-5">
      <h1
        dir="ltr"
        style="line-height:1.333332;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:27pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Chapo.Chat Privacy Policy
        </span>
      </h1>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 11pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We need to have this shit up to cover everyone&rsquo;s ass.&nbsp;
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 11pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We want you to understand how and why Chapo (&ldquo;Chapo,&rdquo;
          &ldquo;we&rdquo; or &ldquo;us&rdquo;) collects, uses, and shares
          information about you when you use our websites and services
          (collectively, the &quot;Services&quot;) or when you otherwise
          interact with us or receive communication from us.&nbsp;
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          What We Collect
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Information You Provide to Us
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We collect information you provide to us directly when you use the
          Services. This includes:
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Account information.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;To create an account, you must provide a username and password.
          Your username is public, and it doesn&rsquo;t have to be related to
          your real name (in fact we strongly discourage it). You may also
          provide an email address. We also store your user account preferences
          and settings.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Content you submit.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;We collect the content you submit to the Services. This includes
          your posts and comments including saved drafts, your messages with
          other users (e.g., private messages, and modmail), and your reports
          and other communications with moderators and with us. Your content may
          include text, links, images, gifs, and videos.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Actions you take.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;We collect information about the actions you take when using the
          Services. This includes your interactions with content, like voting,
          saving, hiding, and reporting. It also includes your interactions with
          other users, such as following, friending, and blocking. We collect
          your interactions with communities, like your subscriptions or
          moderator status.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Other information.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;You may choose to provide other information directly to us. For
          example, we may collect information when you fill out a form,
          participate in Chapo-sponsored activities or otherwise communicate
          with us.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Information We Collect Automatically
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          When you access or use our Services, we may also automatically collect
          information about you. This includes:
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Log and usage data.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;We may log information when you access and use the Services.
          This may include your IP address, user-agent string, browser type,
          operating system, referral URLs, device information (e.g., device
          IDs), pages visited, links clicked, the requested URL, hardware
          settings, and search terms. Except for the IP address used to create
          your account, Chapo will delete any IP addresses collected after 100
          days.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Information collected from cookies and similar technologies.
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          &nbsp;We may receive information from cookies, which are pieces of
          data your browser stores and sends back to us when making requests,
          and similar technologies. We use this information to improve your
          experience, understand user activity, personalize content and
          advertisements, and improve the quality of our Services. For example,
          we store and retrieve information about your preferred language and
          other settings. For more information on how you can disable cookies,
          please see &ldquo;Your Choices&rdquo; below.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          How We Use Information About You
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:11pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We use information about you to:
        </span>
      </p>
      <ul style="margin-top:0;margin-bottom:0;">
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:11pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Provide, maintain, and improve the Services;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Research and develop new services;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Help protect the safety of Chapo and our users, which includes
              blocking suspected spammers, addressing abuse, and enforcing the
              Chapo user agreement and our other policies;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Send you technical notices, updates, security alerts, invoices and
              other support and administrative messages;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:11pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Monitor and analyze trends, usage, and activities in connection
              with our Services
            </span>
          </p>
        </li>
      </ul>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          How Information About You Is Shared
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:11pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          When you use the Services, certain information may be shared with
          other users and the public. For example:
        </span>
      </p>
      <ul style="margin-top:0;margin-bottom:0;">
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:11pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              When you submit content (such as a post or comment or public chat)
              to the Services, any visitors to and users of our Services will be
              able to see that content, the username associated with the
              content, and the date and time you originally submitted the
              content.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              When you send private messages or messages via modmail the
              recipients of those messages will be able to see the content of
              your message, your username, and the date and time the message was
              sent. Moderators may elect to have modmail forwarded to their
              email accounts and, as a result, any modmail received by these
              moderators will be subject to the terms and policies of the
              moderator&rsquo;s email provider.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:11pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              When other users view your profile, they will be able to see
              information about your activities on the Services, such as your
              username, prior posts and comments, etc
            </span>
          </p>
        </li>
      </ul>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 11pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Please note that, even when you delete your account, the posts,
          comments and messages you submit through the Services may still be
          viewable or available on our servers. For more information, see
          &ldquo;Your Choices&rdquo; below.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:11pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Otherwise, we do not share, sell, or give away your personal
          information to third parties unless one of the following circumstances
          applies:
        </span>
      </p>
      <ul style="margin-top:0;margin-bottom:0;">
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:11pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              To comply with the law.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information in response to a request for
              information if we believe disclosure is in accordance with, or
              required by, any applicable law, regulation, legal process or
              governmental request, including, but not limited to, meeting
              national security or law enforcement requirements. To the extent
              the law allows it, we will attempt to provide you with prior
              notice before disclosing your information in response to such a
              request.&nbsp;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              In an emergency.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information if we believe it&#39;s necessary to
              prevent imminent and serious bodily harm to a person.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              To enforce our policies and rights.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information if we believe your actions are
              inconsistent with our user agreements, Code of Conduct, or other
              Chapo policies, or to protect the rights, property, and safety of
              ourselves and others.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              With our affiliates.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information between and among Chapo, and any of
              our parents, affiliates, subsidiaries, and other companies under
              common control and ownership.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              With your consent.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information about you with your consent or at
              your direction.
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:11pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:italic;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              Aggregated or de-identified information.
            </span>
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              &nbsp;We may share information about you that has been aggregated
              or anonymized such that it cannot reasonably be used to identify
              you. For example, we may show the total number of times a post has
              been upvoted without identifying who the visitors were.
            </span>
          </p>
        </li>
      </ul>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Ads
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:12pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Fuck ads.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Security Statement - AKA Warrant Canary
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:12pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We have not placed any backdoors into our software and have not
          received any requests for doing so. Since our launch in July 2020, we
          have received 0 (zero) government requests for information and
          complied with 0 (zero) government requests for information. No
          warrants have ever been served to chapo.chat, or chapo.chat
          contributors in regards to chapo.chat. No searches or seizures of any
          kind have ever been performed on chapo.chat assets. Pay close
          attention to any modifications to the previous paragraph.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Your Choices
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          As a Chapo user, you have choices about how to protect and limit the
          collection, use, and disclosure of information about you.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Accessing and Changing Your Information
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          You can access and change certain information through the Services.
          You can also request a copy of the personal information Chapo
          maintains about you by&nbsp;
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Deleting Your Account
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          You may delete your account information at any time from the&nbsp;
        </span>
        <span style="font-size:11pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          user preferences page
        </span>
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          . You can also submit a request to delete the personal information
          Chapo maintains about you by emailing us. When you delete your
          account, your profile is no longer visible to other users and
          disassociated from content you posted under that account. Please note,
          however, that the posts, comments, and messages you submitted prior to
          deleting your account will still be visible to others unless you first
          delete the specific content. We may also retain certain information
          about you as required by law or for legitimate business purposes after
          you delete your account.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Do Not Track
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Most modern web browsers give you the option to send a Do Not Track
          signal to the websites you visit, indicating that you do not wish to
          be tracked. However, there is no accepted standard for how a website
          should respond to this signal, and we do not take any action in
          response to this signal. Instead, in addition to publicly available
          third-party tools, we offer you the choices described in this policy
          to manage the collection and use of information about you.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Other Information
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Information Security
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We take measures to help protect information about you from loss,
          theft, misuse and unauthorized access, disclosure, alteration, and
          destruction. For example, we use HTTPS while information is being
          transmitted. We also enforce technical and administrative access
          controls to limit which of our contributors have access to non-public
          personal information.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Data Retention
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We store the information we collect for as long as it is necessary for
          the purpose(s) for which we originally collected it. We may retain
          certain information for legitimate purposes or as required by law.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          International Data Transfers
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We are based in the United States and we process and store information
          on servers located in France. We may store information on servers and
          equipment in other countries depending on a variety of factors,
          including the locations of our users and service providers. By
          accessing or using the Services or otherwise providing information to
          us, you consent to the processing, transfer and storage of information
          in and to the U.S. and other countries, where you may not have the
          same rights as you do under local law.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          In connection with Chapo&#39;s processing of personal data received
          from the European Union and Switzerland, we adhere to the EU-U.S. and
          Swiss-U.S. Privacy Shield Program (&ldquo;Privacy Shield&rdquo;) and
          comply with its framework and principles.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Additional Information for EEA Users
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Users in the European Economic Area have the right to request access
          to, rectification of, or erasure of their personal data; to data
          portability in certain circumstances; to request restriction of
          processing; to object to processing; and to withdraw consent for
          processing where they have previously provided consent. These rights
          can be exercised using the information provided under &ldquo;Your
          Choices&rdquo; above or as described in the &ldquo;Other Information -
          Data Subject and Consumer Information Requests&rdquo; section below.
          EEA users also have the right to lodge a complaint with their local
          supervisory authority.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:21pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          As required by applicable law, we collect and process information
          about individuals in the EEA only where we have legal bases for doing
          so. Our legal bases depend on the Services you use and how you use
          them. We process your information on the following legal bases:
        </span>
      </p>
      <ul style="margin-top:0;margin-bottom:0;">
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:11pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              You have consented for us to do so for a specific purpose;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              We need to process the information to provide you the Services,
              including to operate the Services, provide customer support and
              personalized features and to protect the safety and security of
              the Services;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              It satisfies a legitimate interest (which is not overridden by
              your data protection interests), such as preventing fraud,
              ensuring network and information security, enforcing our rules and
              policies, protecting our legal rights and interests, research and
              development;
            </span>
          </p>
        </li>
        <li
          dir="ltr"
          style="list-style-type:disc;font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;"
        >
          <p
            dir="ltr"
            style="line-height:1.38;margin-top:0pt;margin-bottom:11pt;"
          >
            <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
              We need to process your information to comply with our legal
              obligations.
            </span>
          </p>
        </li>
      </ul>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Data Subject and Consumer Information Requests
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Requests for a copy of the information Chapo has about your
          account&mdash;including EU General Data Protection Regulation (GDPR)
          data subject access requests and California Consumer Privacy Act
          (CCPA) consumer information requests&mdash;can be submitted to
          contact@chapo.chat.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          All other data subject and consumer requests under data protection
          laws should be sent via email to contact@chapo.chat from the email
          address that you have verified with your Chapo account.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Before we process a request from you about your personal information,
          we need to verify the request via your access to your Chapo account or
          to a verified email address associated with your Chapo account. You
          may also designate an authorized agent to exercise these rights on
          your behalf. Chapo does not discriminate against users for exercising
          their rights under data protection laws to make requests regarding
          their personal information.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Children
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 21pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Children under the age of 13 are not allowed to create an account or
          otherwise use the Services. Additionally, if you are in the EEA, you
          must be over the age required by the laws of your country to create an
          account or otherwise use the Services, or we need to have obtained
          verifiable consent from your parent or legal guardian.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.44;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 13pt 0pt;"
      >
        <span style="font-size:15pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Changes to This Policy
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 11pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          We may change this Privacy Policy from time to time. If we do, we will
          let you know by revising the date at the top of the policy. If we make
          a change to this policy that, in our sole discretion, is material, we
          will provide you with additional notice (such as adding a statement to
          c/announcements, the front page of the Services or sending you a
          notification). We encourage you to review the Privacy Policy whenever
          you access or use our Services or otherwise interact with us to stay
          informed about our information practices and the ways you can help
          protect your privacy. By continuing to use our Services after Privacy
          Policy changes go into effect, you agree to be bound by the revised
          policy.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.400004;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 23pt 0pt;"
      >
        <span style="font-size:18pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:700;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          Contact Us
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:0pt;padding:0pt 0pt 11pt 0pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          To send a GDPR data subject request or CCPA consumer request, follow
          the steps in the &ldquo;Other Information - Data Subject and Consumer
          Information Requests&rdquo; section above.
        </span>
      </p>
      <p
        dir="ltr"
        style="line-height:1.38;background-color:#ffffff;margin-top:0pt;margin-bottom:11pt;"
      >
        <span style="font-size:10.5pt;font-family:Arial;color:#31363a;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">
          If you have other questions about this Privacy Policy, please post
          them in the appropriate community
        </span>
      </p>
    </div>
  </div>
);

export default PrivacyPolicy;
